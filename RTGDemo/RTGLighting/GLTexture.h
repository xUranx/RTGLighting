//Copyright � 2018 Kajaani University of Applied Sciences. All rights reserved. See license.txt
#ifndef ROUTA_GL_TEXTURE_H
#define ROUTA_GL_TEXTURE_H

namespace Engine
{
typedef unsigned int GLuint;
struct GLTexture 
{
	GLuint id;
	int    width;
	int    height;
};

}// namespace re
#endif //ROUTA_GL_TEXTURE_H
