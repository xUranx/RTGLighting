#include "ModelDraw.h"
//#include <fbxsdk/fbxsdk.h>
#include "obj_loader.h"
#include "Log.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
namespace Engine
{
ModelDraw::ModelDraw()
{
	
}


ModelDraw::~ModelDraw()
{
}
void ModelDraw::init()
{
	if (m_vao == 0)
	{
		glGenVertexArrays(1, &m_vao);
	}
	glBindVertexArray(m_vao);

	if (m_mvbo == 0)
	{
		glGenBuffers(1, &m_mvbo);
	}
	glBindBuffer(GL_ARRAY_BUFFER, m_mvbo);

	if (m_ibo == 0)
	{
		glGenBuffers(1, &m_ibo);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void *)offsetof(MeshVertex, pos));
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void *)offsetof(MeshVertex, uv));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void *)offsetof(MeshVertex, normal));

	glBindVertexArray(0);
}

void ModelDraw::addMesh(std::string name, std::string filename)
{

	/*FbxManager* lSdkManager = FbxManager::Create();

	FbxIOSettings *ios = FbxIOSettings::Create(lSdkManager, IOSROOT);
	lSdkManager->SetIOSettings(ios);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_MATERIAL, false);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_TEXTURE, false);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_LINK, false);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_SHAPE, false);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_GOBO, false);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_ANIMATION, false);
	(*(lSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);

	FbxImporter* lImporter = FbxImporter::Create(lSdkManager, "");

	bool lImportStatus = lImporter->Initialize(filename.c_str(), -1, lSdkManager->GetIOSettings());

	if (!lImportStatus)
		fatal_error("Call to FbxImporter::Initialize() failed.\n Error returned : %s\n\n", lImporter->GetStatus().GetErrorString());
	
	// Create a new scene so it can be populated by the imported file.
	FbxScene* lScene = FbxScene::Create(lSdkManager, name.c_str());

	// Import the contents of the file into the scene.
	lImporter->Import(lScene);

	lImporter->Destroy();

	FbxNode* lMeshNode = FbxNode::Create(lScene, "meshNode");

	FbxMesh* lMesh = FbxMesh::Create(lScene, "");

	lMeshNode->SetNodeAttribute(lMesh);

	FbxVector4* vertex = lMesh->GetControlPoints();

	lMesh->GetTextureUVIndex();

	Mesh m;
	m.vert.resize(lMesh->GetControlPointsCount());
	for (int i = 0; i < lMesh->GetControlPointsCount();i++)
	{
		m.vert.push_back({ (float)vertex[i].mData[0], (float)vertex[i].mData[1], (float)vertex[i].mData[2], (float)vertex[i].mData[3] });
	}*/

	Mesh m;
	IndexedModel model = OBJModel(filename).ToIndexedModel();
	//loadMesh(filename, &m);
	m.vert.reserve(model.positions.size());
	for (int i = 0; i < model.positions.size(); i++)
	{
		MeshVertex mesh;
		mesh.pos = model.positions[i];
		mesh.uv = model.texCoords[i];
		mesh.normal = model.normals[i];
		m.vert.push_back(mesh);
	}
	m.indices.reserve(model.indices.size());
	for (auto c : model.indices)
	{
		m.indices.push_back(c);
	}
	meshList.insert(std::make_pair(name, m));
	
}
void ModelDraw::beging()
{
	drawList.clear();
}
void ModelDraw::draw(std::string name, glm::vec3 pos, glm::vec3 angle, glm::vec3 scale, ColourRGBA8 col, Material material, GLuint texture, MeshType type)
{
	glm::mat4 matt{ 1.f };
	glm::mat4 mat = glm::translate(matt, pos);
	glm::quat mquat(glm::radians(angle));
	glm::mat4 rot = glm::toMat4(mquat);
	glm::mat4 mscale = glm::scale(matt, scale);
	matt = mat * rot * mscale;
	drawList.emplace_back(name, col, texture, material, matt, type);
}
void ModelDraw::end()
{
} 
void ModelDraw::render(GLSLProgram& proc)
{
	glBindVertexArray(m_vao);
	for(int i = 0; i<drawList.size();i++)
	{
		Mesh mesh = meshList.find(drawList[i].m_name)->second;
		
		GLint mLoc = proc.getUniformLoc("model");

		glUniformMatrix4fv(mLoc, 1, GL_FALSE, &(drawList[i].m[0][0]));
	
		GLint cLoc = proc.getUniformLoc("collor");
		float col[4];
		col[0] = drawList[i].m_colour.r / 255.0f;
		col[1] = drawList[i].m_colour.g / 255.0f;
		col[2] = drawList[i].m_colour.b / 255.0f;
		col[3] = drawList[i].m_colour.a / 255.0f;
		glUniform4fv(cLoc, 1, &(col[0]));
		
		glBindTexture(GL_TEXTURE_2D, drawList[i].m_texture);

		proc.uniform3fv("material.ambient", drawList[i].m_material.ambient);
		proc.uniform3fv("material.diffuse", drawList[i].m_material.diffuse);
		proc.uniform3fv("material.specular", drawList[i].m_material.specular);
		proc.uniform1fv("material.shininess", drawList[i].m_material.shininess);

		GLint tLoc = proc.getUniformLoc("meshType");
		glUniform1i(tLoc, drawList[i].m_type);

		glDrawElementsBaseVertex(GL_TRIANGLES, mesh.indices.size() , GL_UNSIGNED_INT, (void*)(sizeof(GLuint)*mesh.lastI), mesh.offset);
		
		GLenum err;
		if ((err = glGetError()) != GL_NO_ERROR)
		{
			const std::string *str = new std::string(reinterpret_cast<const char*>(glewGetErrorString(err)));
			fatal_error(str->c_str());
		}

	}
	glBindVertexArray(0);
}
//private
void ModelDraw::updateBuffer()
{
	indices.clear();
	GLuint ind = 0;
	GLuint ofs = 0;
	int size = 0;
	for (auto& c : meshList)
	{
		size += c.second.vert.size();
		for (auto i : c.second.indices)
		{
			indices.push_back(i);
		}
		c.second.offset = ofs;
		c.second.lastI = ind;
		ind += c.second.indices.size();
		ofs += c.second.vert.size();
	}
	glBindBuffer(GL_ARRAY_BUFFER, m_mvbo);
	glBufferData(GL_ARRAY_BUFFER, size*sizeof(MeshVertex), nullptr, GL_DYNAMIC_DRAW);
	GLuint offset = 0;
	for (auto c : meshList)
	{
		glBufferSubData(GL_ARRAY_BUFFER, offset*sizeof(MeshVertex), c.second.vert.size() * sizeof(MeshVertex), c.second.vert.data());
		offset += c.second.vert.size();
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), nullptr, GL_DYNAMIC_DRAW);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indices.size() * sizeof(GLuint), indices.data());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
}
}