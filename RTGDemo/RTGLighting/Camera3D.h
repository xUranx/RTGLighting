#pragma once
#include <glm/glm.hpp>
namespace Engine
{
class Camera3D
{
public:
	Camera3D();
	~Camera3D();

	void init(float fov, float aspect, float zNear, float zFar);

	void setPos(glm::vec3 pos) { m_pos = pos; m_needsUpdate = true; }
	glm::vec3 getPos() { return m_pos; m_needsUpdate = true;}

	glm::mat4 getViewMat() { return m_viewMat; }
	glm::mat4 getCameraMatrix() { return m_perspectiveMatrix*m_viewMat; }

	void moveForward(float value) { m_pos += m_dir*value; m_needsUpdate = true; }

	void moveRight(float value) { m_pos += m_right*value; m_needsUpdate = true; }

	void addToPos(glm::vec3 pos) { m_pos += pos; m_needsUpdate = true;}

	void rotateCamera(float sensity, glm::vec2 mot) { m_angle += sensity*mot; m_needsUpdate = true; m_needsUpdate = true;}

	void totateAngle(glm::vec2 angle) { m_angle = angle; m_needsUpdate = true;}

	void addToAngle(glm::vec2 angle) { m_angle += angle; m_needsUpdate = true;}

	void setForward(glm::vec3 dir) { m_dir = dir; m_needsUpdate = true; setDir = true; }
	void addToForward(glm::vec3 dir) { m_dir += dir; m_needsUpdate = true; }

	void update();

private:
	glm::mat4 m_perspectiveMatrix = glm::mat4(1.0f);
	glm::mat4 m_viewMat = glm::mat4(1.0f);

	glm::vec3 m_pos = glm::vec3(0.0f);

	glm::vec3 m_dir;
	glm::vec3 m_right;

	glm::vec2 m_angle = { 3.14f , 0.0f };

	bool m_needsUpdate = true;
	bool setDir = false;
};
}

