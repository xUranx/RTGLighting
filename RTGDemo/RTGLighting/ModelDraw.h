#pragma once
#include <glew/glew.h>
#include "Vertex.h"
#include <glm/glm.hpp>
#include <vector>
#include <map>
#include <string>
#include "obj_loader.h"
#include "GLSLProgram.h"
#include <unordered_map>
namespace Engine
{

enum MeshType
{
	Normal = 0, Lamp
};
struct MeshVertex
{
	glm::vec3 pos;
	glm::vec2 uv;
	glm::vec3 normal;
};
struct Mesh
{
	std::vector<MeshVertex> vert;
	GLuint offset = 0;
	GLuint lastI = 0;
	std::vector<GLuint> indices;
};

struct RBatch
{
	RBatch(std::string name, ColourRGBA8 colour, GLuint _Texture, Material material, glm::mat4 mat, MeshType type) : 
		m_name(name),m_colour(colour), m_texture(_Texture), m_material(material), m(mat), m_type(type){}
	std::string m_name;
	ColourRGBA8 m_colour;
	GLuint m_texture = 0;
	Material m_material;
	glm::mat4 m;
	MeshType m_type;
};

class ModelDraw
{
public:
	ModelDraw();
	~ModelDraw();

	void init();
	void addMesh(std::string name, std::string filename);
	void beging();
	void draw(std::string name, glm::vec3 pos, glm::vec3 angle, glm::vec3 scale, ColourRGBA8 col, Material material, GLuint texture, MeshType type = Normal);
	void end();
	void render(GLSLProgram& proc);

	void updateBuffer();
private:

	bool nUpdate = true;


	GLuint m_vao = 0;
	GLuint m_mvbo = 0;
	GLuint m_ibo = 0;
	std::vector<GLuint> indices;
	std::map<std::string, Mesh> meshList;
	std::vector<RBatch> drawList;
};
}