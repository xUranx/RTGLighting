#define STB_IMAGE_IMPLEMENTATION
#include "GLTexture.h"
#include "ImageLoader.h"
#include "IOManager.h"
#include <stb/stb_image.h>

#if defined(WIN32) && !defined(_FES)
#include <glew/glew.h>
#elif defined(__ANDROID__) || defined(_FES)
#include <GLES3/gl3.h>
#elif defined(__linux__)
#include <GL/glew.h>
#endif


#ifndef WIN32
#include <sstream>
namespace std
{
template <typename T>
string to_string(T value)
{
	ostringstream os;
	os << value;
	return os.str();
}
}
#endif



namespace Engine 
{

GLTexture ImageLoader::loadImage(std::string filepath)
{

	GLTexture texture = {};
	std::vector<unsigned char> in;
	unsigned char* out;

	int width;
	int height;
	int bpp;
	if (!IOManager::readFileToBuffer(filepath, in))
		printf("Image not found");
	out = stbi_load_from_memory(in.data(), in.size(),&width, &height, &bpp, STBI_rgb_alpha);
	//out = stbi_load(filepath.c_str(), &width, &height, &bpp, STBI_rgb_alpha);
	glGenTextures(1, &(texture.id));
	glBindTexture(GL_TEXTURE_2D, texture.id);
	glTexImage2D(GL_TEXTURE_2D, 0, 
		(4==bpp) ? GL_RGB : GL_RGBA,
		width, height, 0,
		(4==bpp) ? GL_RGB : GL_RGBA, 
		GL_UNSIGNED_BYTE, out);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	texture.width  = width;
	texture.height = height;

	return texture;
}
}// namespace re