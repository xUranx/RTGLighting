#include "MainGame.h"
#include "Log.h"
#include <iostream>
#include <glew/glew.h>
#include "Camera3D.h"
#include "ResourceManager.h"
using namespace Engine;
bool cameraControll = false;
MainGame::MainGame(): sWidth(800.0f), sHeight(600.0f)
{
}


MainGame::~MainGame()
{
}


void MainGame::run()
{
	if (!window.init("RTGDemo", sWidth, sHeight, 0))
	{
		fatal_error("Failed to init window");
	}
	else
	{	
		
		glClearColor(35.0f / 255.0f, 130.0f / 255.0f, 117.0f / 255.0f, 1.0f);
		glClearDepth(1.0);
		glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
		//glDepthFunc(GL_LESS);    // Set the type of depth-test
		
		//glShadeModel(GL_SMOOTH);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		SDL_ShowCursor(SDL_DISABLE);
		SDL_SetRelativeMouseMode(SDL_TRUE);

		cam.init(45.0f, sWidth / sHeight, 0.1f, 1000.0f);
		cam.setPos({ 0,0,7.0f });
		sbatch.init();
		m_glsl.compileShaders("Assets/Shaders/shader.vert", "assets/Shaders/shader.frag");
		m_glsl.addAtribute("vertexPosition");
		m_glsl.addAtribute("vertexUV");
		m_glsl.addAtribute("vertexNormal");
		m_glsl.linkShaders();

		gLoop();
	}

	window.close();//Free resources and close SDL
}


void MainGame::gLoop()
{
	
	//Main loop flag
	bool quit = false;

	SDL_Event e;


	sbatch.addMesh("Monkey", "Assets/Models/Monkey.obj");
	sbatch.addMesh("Sphere", "Assets/Models/Sphere.obj");
	sbatch.addMesh("Torus", "Assets/Models/Torus.obj");
	sbatch.addMesh("InvCube", "Assets/Models/InverseCube.obj");
	sbatch.updateBuffer();
	drawGame();
	bool focus = true;
	while (!quit)
	{
		window.fpsCounter();
	
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{	
			switch (e.window.event)
			{
			case SDL_WINDOWEVENT_ENTER:
				m_mouseFocus = true;
				break;

				//Mouse left window
			case SDL_WINDOWEVENT_LEAVE:
				m_mouseFocus = false;
				break;

				//Window has keyboard focus
			case SDL_WINDOWEVENT_FOCUS_GAINED:
				m_keyboardFocus = true;
				break;

				//Window lost keyboard focus
			case SDL_WINDOWEVENT_FOCUS_LOST:
				m_keyboardFocus = false;
				break;
			}
			if (m_keyboardFocus)
			{
				//User requests quit
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				if (e.type == SDL_KEYDOWN)
				{
					inputManager.pressKey(e.key.keysym.sym);
				}
				if (e.type == SDL_KEYUP)
				{
					inputManager.releaseKey(e.key.keysym.sym);
				}
				if (e.type == SDL_MOUSEMOTION)
				{
					if (cameraControll)
					{
						cam.rotateCamera(0.0015f, { -e.motion.xrel, -e.motion.yrel });
						SDL_WarpMouseInWindow(window.gWindow, sWidth / 2, sHeight / 2);
					}
				}
			}


		}
		processInput();
		if (inputManager.isKeyPressed(SDLK_ESCAPE)) quit = true;
		drawGame();

	}
}

void MainGame::processInput()
{
	const float CamSpeed = 0.2f;
	const float ScalSpeed = 0.2f;
	if (cameraControll)
	{
		if (inputManager.isKeyPressed(SDLK_w))
			cam.moveForward(CamSpeed);
		if (inputManager.isKeyPressed(SDLK_s))
			cam.moveForward(-CamSpeed);
		if (inputManager.isKeyPressed(SDLK_d))
			cam.moveRight(CamSpeed);
		if (inputManager.isKeyPressed(SDLK_a))
			cam.moveRight(-CamSpeed);
	}
	if (inputManager.isKeyPressed(SDLK_1))
		lightType = 0;
	if (inputManager.isKeyPressed(SDLK_2))
		lightType = 1;
	if (inputManager.isKeyPressed(SDLK_3))
		lightType = 2;
}
glm::vec3 mAngle(0.0f);
void MainGame::drawGame()
{
	//glClearDepth(1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::vec3 direction = cam.getPos();
	if (!cameraControll)
	{
		if (cameraMode == 1)
			cam.addToPos({ 0.04,0.0,0.0 });
		else
			cam.addToPos({ -0.04,0.0,0.0 });

		if (cam.getPos().x > 10)
			cameraMode = 0;
		else if (cam.getPos().x < -10)
			cameraMode = 1;
		cam.setForward(-direction);
	}
	cam.update();
	m_glsl.use();

	GLint pLoc = m_glsl.getUniformLoc("VP");
	glm::mat4 camMat = cam.getCameraMatrix();
	glUniformMatrix4fv(pLoc, 1, GL_FALSE, &(camMat[0][0]));

	glActiveTexture(GL_TEXTURE0);
	GLint textLoc = m_glsl.getUniformLoc("Textu");
	glUniform1i(textLoc, 0);
	sbatch.beging();

	GLint typeLoc = m_glsl.getUniformLoc("bLight.type");
	glUniform1i(typeLoc, lightType);
	glm::vec3 lightPos(0.0f, 0.0f, 8.0f);
	m_glsl.uniform3fv("lightPos", lightPos);
	glm::vec3 lightDir;
	if (lightType != 1)
	{
		sbatch.draw("Sphere", lightPos, { 0.0f,0.0f,0.0f }, { 0.5f,0.5f,0.5f }, { 255,255,255,255 }, Material(), NULL, Lamp);
		lightDir = glm::vec3(0, 0, -1);
	}
	else
	{
		lightDir = glm::vec3(-0.2f, -1.0f, -0.3f);
	}

	m_glsl.uniform3fv("lightDirection", lightDir);
	glm::vec3(-0.2f, -1.0f, -0.3f);
	m_glsl.uniform3fv("bLight.ambient", glm::vec3(0.3f));


	m_glsl.uniform3fv("bLight.diffuse", glm::vec3(0.5f));

	m_glsl.uniform3fv("bLight.specular", glm::vec3(0.5f));

	m_glsl.uniform1fv("bLight.constant", 1.0f);

	m_glsl.uniform1fv("bLight.linear", 0.022f);

	m_glsl.uniform1fv("bLight.quadratic", 0.0019f);

	m_glsl.uniform1fv("cutOff", glm::cos(glm::radians(2.0f)));
	m_glsl.uniform1fv("outerCutOff", glm::cos(glm::radians(10.0f)));


	GLuint text = Engine::ResourceManager::getTexture("Assets/Textures/bricks.jpg").id;

	Material mate;
	mate.ambient = glm::vec3(0.1f);
	mate.diffuse = glm::vec3(0.8f);
	mate.specular = glm::vec3(0.1f);
	mate.shininess = 0.1f;

	Material mater;
	mater.ambient = glm::vec3(0.1f);
	mater.diffuse = glm::vec3(0.2f);
	mater.specular = glm::vec3(0.8f);
	mater.shininess = 32.0f;

	if (mAngle.y > 360 || mAngle.y < -360)
	{
		mAngle.y = 0;
	}

	mAngle.y += 0.5;

	sbatch.draw("Monkey", { 0.0f,0.0f,0.0f }, mAngle, { 1.0f,1.0f,1.0f }, { 255,255,255,255 }, mate, text);
	sbatch.draw("Torus", { 0.0f,1.3f,0.0f }, { 0.0f,0.0f,0.0f }, { 1.0f,1.0f,1.0f }, { 255,255,255,255 }, mate, text);
	sbatch.draw("InvCube", { 0.0f,0.0f,0.0f }, { 0.0f,0.0f,0.0f }, { 4.0f,4.0f,4.0f }, { 255,255,255,255 }, mater, text);

	sbatch.end();
	sbatch.render(m_glsl);
	glBindTexture(GL_TEXTURE_2D, 0);
	m_glsl.unuse();
	SDL_GL_SwapWindow(window.gWindow);
}