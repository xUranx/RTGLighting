#pragma once
#include <string>
#if defined(WIN32) && !defined(_FES)
#include <glew/glew.h>
#elif defined(__ANDROID__) || defined(_FES)
#include <GLES3/gl3.h>
#endif
#include <glm/glm.hpp>
namespace Engine {
	class GLSLProgram
	{
	public:
		GLSLProgram();
		~GLSLProgram();


		bool compileShaders(std::string vertexShaderFilePath, std::string fragmentShaderFilePath);
		bool compileShaders(std::string vertexShaderFilePath,std::string geometryShaderFilePath, std::string fragmentShaderFilePath);

		void compileShadersFromSource(const char* vertexSource, const char* fragmentSource);
		void compileShadersFromSource(const char* vertexSource, const char* geometrySource, const char* fragmentSource);

		bool linkShaders();

		void addAtribute(const std::string& attName);

		void use();
		void unuse();

		GLint getUniformLoc(const std::string& uniformName);


		void uniform3fv(char* location, glm::vec3 value) { glUniform3fv(getUniformLoc(location), 1, &value[0]); }
		void uniform1fv(char* location, float value) { glUniform1fv(getUniformLoc(location), 1, &value); }

		void dispose();

	private:

		int numAttribute;

		bool compileShader(const char* Source, std::string t, GLuint id);

		GLuint programID;

		GLuint vertexShaderID;
		GLuint geometryShaderID;
		GLuint fragmentShaderID;
	};

}