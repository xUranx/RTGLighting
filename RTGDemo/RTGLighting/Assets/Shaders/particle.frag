#version 330 core

in vec4 fragColor;
in vec2 fragUV;

out vec4 colour;

uniform sampler2D Textu;


void main()
{
	
	vec4 textureColour = texture(Textu, fragUV);
	if(textureColour == vec4(0,0,0,1))
	{
		colour = fragColor;
	}
	else
	{
		colour = textureColour * fragColor;
	}
}