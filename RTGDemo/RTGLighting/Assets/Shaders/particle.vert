#version 330 core

layout(location = 0) in vec3 squareVertices;
layout(location = 1) in vec4 xyzs; 
layout(location = 2) in vec4 color; 

out vec4 fragColor;
out vec2 fragUV;

uniform mat4 viewMat;
uniform vec3 CameraRight;
uniform vec3 CameraUp;

void main()
{
    float pSize = xyzs.w;
    vec3 pos = xyzs.xyz;

    vec3 vertex = pos
		+ CameraRight * squareVertices.x * pSize
		+ CameraUp * squareVertices.y * pSize;

    gl_Position = viewMat * vec4(vertex,1.0);
    fragColor = color;
    fragUV = squareVertices.xy + vec2(0.5, 0.5);
}
