#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

out vec3 fragmentPos;
out vec2 fragmentUV;
out vec3 fragmentNormal;

uniform mat4 VP;
uniform mat4 model;

void main()
{
	
	gl_Position = (VP * model * vec4(vertexPosition, 1));

	fragmentPos = (model * vec4(vertexPosition, 1)).xyz;
	fragmentUV = vec2(vertexUV.x,1.0 - vertexUV.y);
	fragmentNormal = mat3(transpose(inverse(model)))*vertexNormal;
}