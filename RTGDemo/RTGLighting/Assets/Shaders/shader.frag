#version 330 core

in vec3 fragmentPos;
in vec2 fragmentUV;
in vec3 fragmentNormal;

out vec4 colour;

struct Material 
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

uniform Material material;

struct BaseLight
{
	vec3 ambient;
    vec3 diffuse;
    vec3 specular;

	float constant;
    float linear;
    float quadratic;

	int type;
};

uniform BaseLight bLight;

uniform sampler2D Textu;
uniform vec4 collor;

uniform vec3 lightPos;
uniform float cutOff;
uniform float outerCutOff;
uniform vec3 lightDirection;
uniform vec3 camPos;
uniform vec3 viewPos;

uniform int meshType;

void main()
{
	
	if(!(meshType == 1))
	{
		vec3 lightDir = normalize(-lightDirection);
		
		vec3 ambient = (bLight.ambient * material.ambient);

		vec3 norm = normalize(fragmentNormal);
		if(bLight.type == 0 || bLight.type == 2)
		{ 
			lightDir = normalize(lightPos - fragmentPos);
		}

		float diff = max(dot(norm, lightDir), 0.0);

		vec3 diffuse = (bLight.diffuse*(diff*material.diffuse));

		//spec
		vec3 viewDir = normalize(viewPos - fragmentPos);
		//vec3 reflectDir = reflect(-lightDir, norm);  
		vec3 halfwayDir = normalize(lightDir + viewDir);

		float spec = pow(max(dot(viewDir, halfwayDir), 0.0), material.shininess);
		vec3 specular = (bLight.specular) * (spec * material.specular);

		if(bLight.type == 0 || bLight.type == 2)
		{
			if(bLight.type == 2)
			{
				float theta = dot(lightDir, normalize(-lightDirection)); 
				float epsilon = (cutOff - outerCutOff);
				float intensity = clamp((theta - outerCutOff) / epsilon, 0.0, 1.0);
				diffuse  *= intensity;
				specular *= intensity;
			}

			float dist    = length(lightPos - fragmentPos);
			float attenuation = 1.0 / (bLight.constant + bLight.linear * dist + 
									bLight.quadratic * (dist * dist));    
			ambient  *= attenuation; 
			diffuse  *= attenuation;
			specular *= attenuation;

		}
		vec3 result = ambient + diffuse + specular;

		vec4 textureColour = texture(Textu, fragmentUV);
		if(textureColour == vec4(0,0,0,1))
		{
			colour = vec4(result*collor.xyz,collor.w);
		}
		else
		{
			colour = vec4(result*(textureColour * collor).xyz,collor.w); 
		}
	}
	else
	{
		vec4 textureColour = texture(Textu, fragmentUV);
		if(textureColour == vec4(0,0,0,1))
		{
			colour = collor;
		}
		else
		{
			colour = textureColour * collor;
		}
	}
}