#pragma once
#include <map>
#include "GLTexture.h"

namespace Engine
{
class TextureCache
{
public:
	TextureCache() {}
	~TextureCache() {}
	GLTexture getTexture(char* path);
private:
	std::map<char*, GLTexture> textureMap;
};

class ResourceManager
{
public:

	static GLTexture getTexture(char* path);
private:
	static TextureCache textureCache;
};
}

