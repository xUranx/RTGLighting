//Copyright � 2018 Kajaani University of Applied Sciences. All rights reserved. See license.txt
#ifndef ROUTA_IMAGE_LOADER_H
#define ROUTA_IMAGE_LOADER_H
#include <string>
namespace Engine
{
struct GLTexture;
class ImageLoader
{
public:
	static GLTexture loadImage(std::string filepath);
};

}// namespace re
#endif //ROUTA_IMAGE_LOADER_H
