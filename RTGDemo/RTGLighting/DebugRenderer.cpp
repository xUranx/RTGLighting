#include "DebugRenderer.h"
#define _USE_MATH_DEFINES
#include <math.h>

const char* VERT_SRC = R"(#version 330
	//The vertex shader operates on each vertex


in vec3 vertexPosition;
in vec4 vertexColor;

out vec3 fragmentPosition;
out vec4 fragmentColor;

uniform mat4 P;

void main() {
	//Set the x,y position on the screen
	gl_Position = P * vec4(vertexPosition, 1.0);

	//Indicate that the coordinates are normalized

	fragmentPosition = vertexPosition;

	fragmentColor = vertexColor;
})";

const char* FRAG_SRC = R"(#version 330
	//The fragment shader operates on each pixel in a given polygon

in vec3 fragmentPosition;
in vec4 fragmentColor;

//This is the 3 component float vector that gets outputted to the screen
//for each pixel.
out vec4 color;

void main() {

	color = fragmentColor;
})";
namespace Engine {

	DebugRenderer::DebugRenderer()
	{
	}


	DebugRenderer::~DebugRenderer()
	{
		dispose();
	}

	void DebugRenderer::init()
	{

		shaderProc.compileShadersFromSource(VERT_SRC, FRAG_SRC);
		shaderProc.addAtribute("vertexPos");
		shaderProc.addAtribute("vertexColor");
		shaderProc.linkShaders();
#ifdef WIN32
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
#endif

		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(DebugVertex), (void *)offsetof(DebugVertex, position));
		glVertexAttribPointer(1, 4,GL_UNSIGNED_BYTE, GL_TRUE, sizeof(DebugVertex), (void *)offsetof(DebugVertex, colour));
#ifdef WIN32
		glBindVertexArray(0);
#else
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
#endif // WIN32

		
	}

	void DebugRenderer::end()
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, debugVertex.size() * sizeof(DebugVertex), nullptr, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, debugVertex.size() * sizeof(DebugVertex), debugVertex.data());
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), nullptr, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indices.size() * sizeof(GLuint), indices.data());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		numElements = indices.size();
		indices.clear();
		debugVertex.clear();
	}

	glm::vec2 rotatePoint(glm::vec2 pos, float angle)
	{
		glm::vec2 newv;
		newv.x = pos.x * cos(angle) - pos.y * sin(angle);
		newv.y = pos.x * sin(angle) + pos.y * cos(angle);
		return newv;
	}

	/*void DebugRenderer::drawBox(const glm::vec4 & destRect, const ColourRGBA8 colour, float angle)
	{
		int i = debugVertex.size();
		debugVertex.resize(debugVertex.size() + 4);

		glm::vec2 hlfDims(destRect.z / 2.0f, destRect.w / 2.0f);

		glm::vec2 tl(-hlfDims.x, hlfDims.y);
		glm::vec2 bl(-hlfDims.x, -hlfDims.y);
		glm::vec2 br(hlfDims.x, -hlfDims.y);
		glm::vec2 tr(hlfDims.x, hlfDims.y);

		glm::vec2 posOff(destRect.x, destRect.y);

		//RotatePoints
		debugVertex[i].position = rotatePoint(tl, angle) + hlfDims + posOff;
		debugVertex[i + 1].position = rotatePoint(bl, angle) + hlfDims + posOff;
		debugVertex[i + 2].position = rotatePoint(br, angle) + hlfDims + posOff;
		debugVertex[i + 3].position = rotatePoint(tr, angle) + hlfDims + posOff;

		for (int j = i; j < i + 4; j++)
		{
			debugVertex[j].colour = colour;
		}
		indices.reserve(indices.size() + 8);

		indices.push_back(i);
		indices.push_back(i + 1);

		indices.push_back(i + 1);
		indices.push_back(i + 2);

		indices.push_back(i + 2);
		indices.push_back(i + 3);

		indices.push_back(i + 3);
		indices.push_back(i);

	}*/

	void DebugRenderer::drawCube(const glm::vec3& pos, const glm::vec3& rect, const ColourRGBA8 colour)
	{
		int i = debugVertex.size();
		debugVertex.resize(debugVertex.size() + 8);

		glm::vec3 hlfDims = rect*0.5f;

		glm::vec3 tlf(-hlfDims.x, hlfDims.y, hlfDims.z);
		glm::vec3 blf(-hlfDims.x, -hlfDims.y, hlfDims.z);
		glm::vec3 brf(hlfDims.x, -hlfDims.y, hlfDims.z);
		glm::vec3 trf(hlfDims.x, hlfDims.y, hlfDims.z);

		glm::vec3 tlb(-hlfDims.x, hlfDims.y, -hlfDims.z);
		glm::vec3 blb(-hlfDims.x, -hlfDims.y, -hlfDims.z);
		glm::vec3 brb(hlfDims.x, -hlfDims.y, -hlfDims.z);
		glm::vec3 trb(hlfDims.x, hlfDims.y, -hlfDims.z);

		debugVertex[i].position = tlf  + pos;
		debugVertex[i + 1].position = blf + pos;
		debugVertex[i + 2].position = brf + pos;
		debugVertex[i + 3].position = trf + pos;
		debugVertex[i + 4].position = tlb + pos;
		debugVertex[i + 5].position = blb + pos;
		debugVertex[i + 6].position = brb + pos;
		debugVertex[i + 7].position = trb + pos;

		for (int j = i; j < i + 8; j++)
		{
			debugVertex[j].colour = colour;
		}
		indices.reserve(indices.size() + 24);

		indices.push_back(i);
		indices.push_back(i + 1);

		indices.push_back(i + 1);
		indices.push_back(i + 2);

		indices.push_back(i + 2);
		indices.push_back(i + 3);

		indices.push_back(i + 3);
		indices.push_back(i);

		indices.push_back(i + 4);
		indices.push_back(i + 5);

		indices.push_back(i + 5);
		indices.push_back(i + 6);

		indices.push_back(i + 6);
		indices.push_back(i + 7);

		indices.push_back(i + 7);
		indices.push_back(i + 4);

		indices.push_back(i );
		indices.push_back(i + 4);

		indices.push_back(i + 1);
		indices.push_back(i + 5);

		indices.push_back(i + 2);
		indices.push_back(i + 6);

		indices.push_back(i + 3);
		indices.push_back(i + 7);
	}

	void DebugRenderer::drawCircle(const glm::vec2 & center, const ColourRGBA8 colour, float radius)
	{
		static const int NUM_VERTS = 100;
		int start = debugVertex.size();
		debugVertex.resize(debugVertex.size()+NUM_VERTS);
		for (int i = 0; i < NUM_VERTS; i++)
		{
			float angle = ((float)i / (float)NUM_VERTS) * (float)M_PI * 2.0f;
			debugVertex[start + i].position.x = cos(angle)*radius + center.x;
			debugVertex[start + i].position.y = sin(angle)*radius + center.y;
			debugVertex[start + i].colour = colour;
		}
		indices.reserve(indices.size() + NUM_VERTS * 2);
		for (int j = 0; j < NUM_VERTS - 1; j++)
		{
			indices.push_back(start + j);
			indices.push_back(start + j + 1);
		}
		indices.push_back(start + NUM_VERTS - 1);
		indices.push_back(start);
	}

	void DebugRenderer::drawLine(const glm::vec3 & p1, const glm::vec3 & p2, const ColourRGBA8 colour)
	{
		int i = debugVertex.size();
		debugVertex.resize(debugVertex.size() + 2);

		debugVertex[i].position = p1;
		debugVertex[i + 1].position = p2;

		for (int j = i; j < i + 2; j++)
		{
			debugVertex[j].colour = colour;
		}
		indices.reserve(indices.size() + 2);

		indices.push_back(i);
		indices.push_back(i + 1);
	}

	void DebugRenderer::render(const glm::mat4& pMat, float lineWidth)
	{
		shaderProc.use();

		GLint pLoc = shaderProc.getUniformLoc("P");
		glUniformMatrix4fv(pLoc, 1, GL_FALSE, &(pMat[0][0]));
		
		glLineWidth(lineWidth);
#ifdef WIN32
		glBindVertexArray(vao);
#else
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
#endif // WIN32

		glDrawElements(GL_LINES, numElements, GL_UNSIGNED_INT, 0);


#ifdef WIN32
		glBindVertexArray(0);
#else
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
#endif // WIN32

		shaderProc.unuse();
	}

	void DebugRenderer::dispose()
	{
		if (vao)
		{
#ifdef WIN32
			glDeleteVertexArrays(1, &vao);
#endif // WIN32
		}
		if (vbo)
		{
			glDeleteBuffers(1, &vbo);
		}
		if (ibo)
		{
			glDeleteBuffers(1, &ibo);
		}
		shaderProc.dispose();
	}

}