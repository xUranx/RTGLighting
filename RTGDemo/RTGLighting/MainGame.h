#pragma once
#include "Window.h"

#include "InputManager.h"
#include "Camera3D.h"
#include "ModelDraw.h"
#include "GLSLProgram.h"
class MainGame
{
public:
	MainGame();
	~MainGame();
	void run();
private:

	float sWidth, sHeight;
	void gLoop();
	void drawGame();
	void processInput();

	int cameraMode;
	int lightType = 0;

	bool m_mouseFocus;
	bool m_keyboardFocus;

	Engine::Window window;
	Engine::Camera3D cam;
	Engine::ModelDraw sbatch;
	Engine::GLSLProgram m_glsl;
	Engine::InputManager inputManager;
};

