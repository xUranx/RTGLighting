#include "ResourceManager.h"
#include "Log.h"
#include "ImageLoader.h"
namespace Engine
{
#define textureMap_ite std::map<char*, GLTexture>::iterator


GLTexture TextureCache::getTexture(char * path)
{
	textureMap_ite it = textureMap.find(path);

	if (it == textureMap.end())
	{
		//LoadTexture
		GLTexture newTexture = ImageLoader::loadImage(path);
		textureMap.insert(std::make_pair(path, newTexture));
		//Message("Loaded new Texture: " + path);
		return newTexture;
	}
	else
	{
		//Message("Used Cahced Texture");
		return it->second;
	}
}

TextureCache ResourceManager::textureCache;
GLTexture ResourceManager::getTexture(char* path)
{
	return textureCache.getTexture(path);
}


}